var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleancss = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
	gulpif = require('gulp-if'),
    del = require('del'),
	stringreplace = require('gulp-string-replace'),
	header = require('gulp-header')
	spritesmith = require('gulp.spritesmith'),
	merge = require('merge-stream'),
	buffer = require('vinyl-buffer');

gulp.task('default', ['clean'], function() {
    gulp.start('styles', 'images');
	
	
});
	
gulp.task('styles', ['textures', 'flairs'], function() {
	return gulp.src([
		'./src/main.scss'
	], {read: true})
	.pipe(sass().on('error', sass.logError)) // Compile css.
	.pipe(stringreplace(new RegExp('@charset \"UTF-8\";', 'g'), ''))
    .pipe(gulp.dest('dist'))
    .pipe(rename({suffix: '.min'}))
    .pipe(cleancss())
    .pipe(gulp.dest('dist'))
    .pipe(notify({ message: 'Styles task complete' }));
});

gulp.task('images', function() {
  return gulp.src('src/images/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
    .pipe(gulp.dest('dist/img'));
});

gulp.task('textures', function () {
	var spriteData = gulp.src('src/textures/used/**/*.png')
	.pipe(spritesmith({
		imgName: 'textures.png',
		cssName: 'temp/textures.scss',
		//cssTemplate: 'handlebarsInheritance.scss.handlebars'
		imgPath: '%%textures%%',
		cssOpts: {
			cssSelector: function (sprite) { return '.flair-' + sprite.name + ''; }
		}
	}));

	var imgStream = spriteData.img
	// DEV: We must buffer our stream into a Buffer for `imagemin` 
		.pipe(buffer())
		.pipe(imagemin())
		.pipe(gulp.dest('dist'));

	// Super aggressive css compression for flairs since obviously they should be compressed since theres a lot yes
	var cssStream = spriteData.css
		/*.pipe(cleancss({
			level: {
				2: { 
					mergeSemantically: true,
					restructureRules: true
				}
			}
		}))*/
		.pipe(gulp.dest('dist'));

	// Return a merged stream to handle both `end` events 
	return merge(imgStream, cssStream);
});

gulp.task('flairs', function () {
	var spriteData = gulp.src('flairs/**/!(header).png')
	.pipe(spritesmith({
		imgName: 'flairs.png',
		cssName: 'temp/flair_positions.css',
		imgPath: '%%flairs%%',
		algorithmOpts: {sort: false},
		cssOpts: {
			cssSelector: function (sprite) { return '.flair-' + sprite.name + ':after'; }
		}
	}));

	var imgStream = spriteData.img
	// DEV: We must buffer our stream into a Buffer for `imagemin` 
		.pipe(buffer())
		.pipe(imagemin())
		.pipe(gulp.dest('dist'));

	// Super aggressive css compression for flairs since obviously they should be compressed since theres a lot yes
	var cssStream = spriteData.css
		.pipe(cleancss({
			level: {
				2: { 
					mergeSemantically: true,
					restructureRules: true
				}
			}
		}))
		.pipe(gulp.dest('dist'));

	// Return a merged stream to handle both `end` events 
	return merge(imgStream, cssStream);
});

gulp.task('clean', function() {
    return del(['dist']);
});


gulp.task('watch', function() {

  // Watch .scss files
  gulp.watch('src/**/*.scss', ['styles']);

  // Watch image files
  gulp.watch('src/images/**/*', ['images']);

});